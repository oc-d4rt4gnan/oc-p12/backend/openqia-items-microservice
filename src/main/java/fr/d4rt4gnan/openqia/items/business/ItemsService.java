package fr.d4rt4gnan.openqia.items.business;


import fr.d4rt4gnan.openqia.items.dao.ItemsRepository;
import fr.d4rt4gnan.openqia.items.model.ItemBean;
import fr.d4rt4gnan.openqia.items.technical.mappers.ItemsMapper;
import fr.d4rt4gnan.openqia.items.api.ItemsApiDelegate;
import fr.d4rt4gnan.openqia.items.model.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


/**
 * @author D4RT4GNaN
 * @since 18/01/2022
 */
@RestController
@Service
public class ItemsService implements ItemsApiDelegate {
    
    /**
     * ItemsRepository dependency.
     */
    private ItemsRepository itemsRepository;
    
    /**
     * ItemsMapper dependency.
     */
    private ItemsMapper itemsMapper;
    
    /**
     * Injection of ItemsMapper dependencies into the service.
     *
     * @param value - The ItemsMapper to inject.
     */
    @Inject
    public void setItemsMapper (ItemsMapper value) {
        this.itemsMapper = value;
    }
    
    /**
     * Injection of ItemsRepository dependencies into the service.
     *
     * @param value - The ItemsRepository to inject.
     */
    @Inject
    public void setItemsRepository (ItemsRepository value) {
        this.itemsRepository = value;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Item> addItem (
            final Item item
    ) {
        ItemBean itemBean = itemsMapper.toItemBean(item);
        ItemBean itemBeanSaved = itemsRepository.save(itemBean);
        return new ResponseEntity<>(itemsMapper.toItem(itemBeanSaved), HttpStatus.CREATED);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Item> getItem (
            final UUID reference
    ) {
        Optional<ItemBean> optionalItemBean = itemsRepository.findById(reference);
        return optionalItemBean.map(itemBean -> new ResponseEntity<>(itemsMapper.toItem(itemBean), HttpStatus.OK))
                       .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<List<Item>> getItems(
            String keyword,
            final Integer limit
    ) {
        if (keyword == null) {
            keyword = "";
        }
        Pageable pageRequest = PageRequest.of(0, limit);
        Page<ItemBean> itemBeanList =
                itemsRepository.findItemBeansByDesignationIgnoringCaseContaining(keyword,pageRequest);
        List<Item> items = itemsMapper.toItems(itemBeanList);
        return new ResponseEntity<>(items, HttpStatus.OK);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Item> updateItem(UUID reference, Item item) {
        Optional<ItemBean> optionalItemBean = itemsRepository.findById(reference);
        if (optionalItemBean.isPresent()) {
            ItemBean itemBean = itemsMapper.updateItemBean(item, optionalItemBean.get());
            ItemBean itemBeanUpdated = itemsRepository.save(itemBean);
            return new ResponseEntity<>(itemsMapper.toItem(itemBeanUpdated), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Item> calculationItemTotalPrice(final UUID reference, final Item item) {
        Item result;
        Optional<ItemBean> optionalItemBean = itemsRepository.findById(reference);
        
        if (optionalItemBean.isPresent()) {
            result = itemsMapper.toNewValue(item, optionalItemBean.get());
        } else {
            result = itemsMapper.toValidItem(item);
        }
        
        result.setTotalPrice(result.getUnitPrice() * result.getQuantity() + result.getUnitPrice() * result.getValueAddedTax());
        
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
}