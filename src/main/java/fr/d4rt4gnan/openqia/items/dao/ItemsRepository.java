package fr.d4rt4gnan.openqia.items.dao;


import fr.d4rt4gnan.openqia.items.model.ItemBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


/**
 * @author D4RT4GNaN
 * @since 18/01/2022
 */
@Repository
public interface ItemsRepository extends PagingAndSortingRepository<ItemBean, UUID> {
    
    Page<ItemBean> findItemBeansByDesignationIgnoringCaseContaining(String designation, Pageable Page);
    
}
