package fr.d4rt4gnan.openqia.items;

import fr.d4rt4gnan.openqia.items.api.ItemsApiController;
import org.springdoc.core.SpringDocUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ItemsMicroserviceApplication {

	public static void main(String[] args) {
		SpringDocUtils.getConfig().addRestControllers(ItemsApiController.class);
		SpringApplication.run(ItemsMicroserviceApplication.class, args);
	}

}
