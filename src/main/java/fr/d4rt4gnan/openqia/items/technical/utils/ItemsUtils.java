package fr.d4rt4gnan.openqia.items.technical.utils;


import fr.d4rt4gnan.openqia.items.technical.utils.enums.ItemAttributesEnum;
import fr.d4rt4gnan.openqia.items.model.Item;


/**
 * @author D4RT4GNaN
 * @since 20/02/2022
 */
public interface ItemsUtils {
    
    /**
     * Checks if the elements are not null.
     *
     * @param item - The object containing the attribute.
     * @param attribute - The attribute containing the value to be tested.
     * @return false if any element is null.
     */
    boolean isValid (Item item, ItemAttributesEnum attribute);
    
    
    
    /**
     * Checks if the elements are not null, if it's a String with length upper than 0 and not only constitute with
     * whitespaces.
     *
     * @param item - The object containing the attribute.
     * @param attribute - The attribute containing the value to be tested.
     * @return true if the element is a String not null and not blank.
     */
    boolean isNotBlank (Item item, ItemAttributesEnum attribute);
    
    
    
    /**
     * Checks if the elements are not null, if it's a positive double.
     *
     * @param item - The object containing the attribute.
     * @param attribute - The attribute containing the value to be tested.
     * @return true if the element is a double not null and not equal to 0.
     */
    boolean doubleIsNotZero (Item item, ItemAttributesEnum attribute);
    
    
    
    /**
     * Checks if the elements are not null, if it's a positive int.
     *
     * @param item - The object containing the attribute.
     * @param attribute - The attribute containing the value to be tested.
     * @return true if the element is an int not null and not equal to 0.
     */
    boolean intIsNotZero (Item item, ItemAttributesEnum attribute);
    
}
