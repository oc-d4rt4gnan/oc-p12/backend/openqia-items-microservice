package fr.d4rt4gnan.openqia.items.technical.mappers.impl;


import fr.d4rt4gnan.openqia.items.model.ItemBean;
import fr.d4rt4gnan.openqia.items.technical.mappers.ItemsMapper;
import fr.d4rt4gnan.openqia.items.technical.utils.ItemsUtils;
import fr.d4rt4gnan.openqia.items.technical.utils.enums.ItemAttributesEnum;
import fr.d4rt4gnan.openqia.items.technical.utils.impl.ItemsUtilsImpl;
import fr.d4rt4gnan.openqia.items.model.Item;
import org.springframework.data.domain.Page;

import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author D4RT4GNaN
 * @since 18/01/2022
 */
@Named(value = "itemsMapper")
public class ItemsMapperImpl implements ItemsMapper {
    
    private static final int    QUANTITY_DEFAULT    = 1;
    private static final double VAT_DEFAULT         = 0.2;
    private static final double UNIT_PRICE_DEFAULT  = 0.0;
    private static final double TOTAL_PRICE_DEFAULT = 0.0;
    
    private ItemsUtils itemsUtils = new ItemsUtilsImpl();
    
    /**
     * @inheritDoc
     */
    @Override
    public Item toItem (final ItemBean itemBean) {
        Item item = new Item();
        
        item.setReference(itemBean.getReference());
        item.setDesignation(itemBean.getDesignation());
        item.setUnitPrice(itemBean.getUnitPrice());
        
        return item;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ItemBean toItemBean (final Item item) {
        ItemBean itemBean = new ItemBean();
    
        itemBean.setReference(item.getReference());
        itemBean.setDesignation(item.getDesignation());
        itemBean.setUnitPrice(item.getUnitPrice());
        
        return itemBean;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public List<Item> toItems (final Page<ItemBean> itemBeans) {
        List<ItemBean> itemBeanList = itemBeans.toList();
        return itemBeanList.stream().map(this::toItem).collect(Collectors.toList());
    }
    
    // TODO : IT
    /**
     * @inheritDoc
     */
    @Override
    public ItemBean updateItemBean (
            final Item item,
            final ItemBean itemBean
    ) {
        itemBean.setDesignation(updateDesignation(item, itemBean));
        itemBean.setUnitPrice(updateUnitPrice(item, itemBean));
        itemBean.setValueAddedTax(updateValueAddedTax(item, itemBean));
        
        return itemBean;
    }
    
    // TODO : IT
    /**
     * @inheritDoc
     */
    @Override
    public Item toNewValue (Item item, ItemBean itemBean) {
        Item result = new Item();
        
        // Keep Information
        result.setReference(itemBean.getReference());
        updateDesignation(item, itemBean);
        
        // Change Information
        updateUnitPrice(item, itemBean);
        updateValueAddedTax(item, itemBean);
        checkQuantity(item);
        checkTotalPrice(item);
        
        return result;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public String updateDesignation (Item item, ItemBean itemBean) {
        return itemsUtils.isNotBlank(item, ItemAttributesEnum.DESIGNATION)
               ? item.getDesignation()
               : itemBean.getDesignation();
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public double updateUnitPrice (Item item, ItemBean itemBean) {
        return itemsUtils.doubleIsNotZero(item, ItemAttributesEnum.UNIT_PRICE)
               ? item.getUnitPrice()
               : itemBean.getUnitPrice();
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public double updateValueAddedTax (Item item, ItemBean itemBean) {
        return itemsUtils.doubleIsNotZero(item, ItemAttributesEnum.VALUE_ADDED_TAX)
               ? item.getValueAddedTax()
               : itemBean.getValueAddedTax();
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public int checkQuantity (Item item) {
        return itemsUtils.intIsNotZero(item, ItemAttributesEnum.QUANTITY)
               ? item.getQuantity()
               : QUANTITY_DEFAULT;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public double checkTotalPrice (Item item) {
        return itemsUtils.doubleIsNotZero(item, ItemAttributesEnum.TOTAL_PRICE)
               ? item.getTotalPrice()
               : TOTAL_PRICE_DEFAULT;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public double checkUnitPrice (Item item) {
        return itemsUtils.doubleIsNotZero(item, ItemAttributesEnum.UNIT_PRICE)
               ? item.getUnitPrice()
               : UNIT_PRICE_DEFAULT;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public double checkValueAddedTax (Item item) {
        return itemsUtils.doubleIsNotZero(item, ItemAttributesEnum.VALUE_ADDED_TAX)
               ? item.getValueAddedTax()
               : VAT_DEFAULT;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public Item toValidItem (final Item item) {
        Item result = new Item();
        
        if (item != null) {
            // Keeping Information
            result.setReference(item.getReference());
            result.setDesignation(item.getDesignation());
            
            // Updated Information
            result.setUnitPrice(checkUnitPrice(item));
            result.setValueAddedTax(checkValueAddedTax(item));
            result.setQuantity(checkQuantity(item));
        } else {
            result.setQuantity(QUANTITY_DEFAULT);
            result.setValueAddedTax(VAT_DEFAULT);
            result.setUnitPrice(UNIT_PRICE_DEFAULT);
        }
        
        return result;
    }
}
