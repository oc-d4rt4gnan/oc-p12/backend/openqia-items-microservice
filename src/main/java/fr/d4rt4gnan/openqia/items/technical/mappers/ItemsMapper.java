package fr.d4rt4gnan.openqia.items.technical.mappers;


import fr.d4rt4gnan.openqia.items.model.ItemBean;
import fr.d4rt4gnan.openqia.items.model.Item;
import org.springframework.data.domain.Page;

import java.util.List;


/**
 * @author D4RT4GNaN
 * @since 18/01/2022
 */
public interface ItemsMapper {
    
    /**
     * Allows to convert the Item object from the database version to
     * the transfer version via the swagger.
     *
     * @param itemBean
     *         - The Item object in database format.
     *
     * @return the Item object in transfer format via swagger.
     */
    Item toItem (ItemBean itemBean);
    
    
    
    /**
     * Convert the Item object from the swagger version to the database
     * version.
     *
     * @param item
     *         - The Item object in transfer format via swagger.
     *
     * @return the Item object in database format.
     */
    ItemBean toItemBean (Item item);
    
    /**
     * Allows you to convert a list of Item objects from the database version to
     * the transfer version via the swagger.
     *
     * @param itemBeans - The Item object list of the database version.
     * @return the Item object list in transfer format via swagger.
     */
    List<Item> toItems (Page<ItemBean> itemBeans);
    
    
    
    /**
     * Update only the different fields and not null.
     *
     * @param item - Input data.
     * @param itemBean - Data in database for comparison.
     * @return an item object in database format with updated fields.
     */
    ItemBean updateItemBean (Item item, ItemBean itemBean);
    
    
    
    /**
     * To get an Item object with quantity, vat and unit price values updated for the calculation according to what was
     * in the database.
     *
     * @param item - Input data.
     * @param itemBean - Data in database for comparison.
     * @return the updated Item.
     */
    Item toNewValue (Item item, ItemBean itemBean);
    
    
    
    /**
     * Update the value of the designation attribute by value from front if it's different from database.
     *
     * @param item - Input data.
     * @param itemBean - Data in database for comparison.
     * @return The updated value of the designation attribute.
     */
    String updateDesignation (Item item, ItemBean itemBean);
    
    
    
    /**
     * Update the value of the unit price attribute by value from front if it's different from database.
     *
     * @param item - Input data.
     * @param itemBean - Data in database for comparison.
     * @return The updated value of the unit price attribute.
     */
    double updateUnitPrice (Item item, ItemBean itemBean);
    
    
    
    /**
     * Update the value of the value added tax attribute by value from front if it's different from database.
     *
     * @param item - Input data.
     * @param itemBean - Data in database for comparison.
     * @return The updated value of the value added tax attribute.
     */
    double updateValueAddedTax (Item item, ItemBean itemBean);
    
    
    
    /**
     * Check that the quantity exists, otherwise the default value is assigned.
     *
     * @param item - Input data.
     * @return the existing or the default value.
     */
    int checkQuantity (Item item);
    
    
    
    /**
     * Check that the total price exists, otherwise the default value is assigned.
     *
     * @param item - Input data.
     * @return the existing or the default value.
     */
    double checkTotalPrice (Item item);
    
    
    
    /**
     * Check that the unit price exists, otherwise the default value is assigned.
     *
     * @param item - Input data.
     * @return the existing or the default value.
     */
    double checkUnitPrice (Item item);
    
    
    
    /**
     * Check that the value added tax exists, otherwise the default value is assigned.
     *
     * @param item - Input data.
     * @return the existing or the default value.
     */
    double checkValueAddedTax (Item item);
    
    
    
    /**
     * Allows you to use the values sent if they are not null, otherwise assign them by default.
     *
     * @param item - Input data.
     * @return the updated Item.
     */
    Item toValidItem (Item item);
    
}
