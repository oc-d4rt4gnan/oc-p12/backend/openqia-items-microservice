package fr.d4rt4gnan.openqia.items.technical.utils.impl;


import fr.d4rt4gnan.openqia.items.technical.utils.ItemsUtils;
import fr.d4rt4gnan.openqia.items.technical.utils.enums.ItemAttributesEnum;
import fr.d4rt4gnan.openqia.items.model.Item;

import javax.inject.Named;


/**
 * @author D4RT4GNaN
 * @since 20/02/2022
 */
@Named(value = "itemsUtils")
public class ItemsUtilsImpl implements ItemsUtils {
    
    /**
     * @inheritDoc
     */
    @Override
    public boolean isValid (Item item, ItemAttributesEnum attribute) {
        return item != null && attribute != null && attribute.getValue(item) != null;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public boolean isNotBlank (Item item, ItemAttributesEnum attribute) {
        String type = attribute.getType();
        Object value = attribute.getValue(item);
        
        boolean isValid = isValid(item, attribute);
        boolean isString = String.class.getName().equals(type);
        boolean isNotEmpty = String.valueOf(value).length() > 0;
        boolean containsOnlyBlankSpaces = String.valueOf(value).matches("\\s*");
        
        return isValid
               && isString
               && isNotEmpty
               && !containsOnlyBlankSpaces;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public boolean doubleIsNotZero (Item item, ItemAttributesEnum attribute) {
        return isValid(item, attribute) && double.class.getName().equals(attribute.getType()) &&
               ((double) attribute.getValue(item)) > 0.0;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public boolean intIsNotZero (Item item, ItemAttributesEnum attribute) {
        return isValid(item, attribute) && int.class.getName().equals(attribute.getType()) && ((int) attribute.getValue(
                item)) > 0;
    }
    
}
