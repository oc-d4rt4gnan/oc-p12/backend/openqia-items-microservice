package fr.d4rt4gnan.openqia.items.technical.utils.enums;


import fr.d4rt4gnan.openqia.items.model.Item;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;


public enum ItemAttributesEnum {
    REFERENCE ("getReference", UUID.class.getName()),
    
    DESIGNATION ("getDesignation", String.class.getName()),
    
    UNIT_PRICE ("getUnitPrice", double.class.getName()),
    
    QUANTITY ("getQuantity", int.class.getName()),
    
    VALUE_ADDED_TAX ("getValueAddedTax", double.class.getName()),
    
    TOTAL_PRICE ("getTotalPrice", double.class.getName());
    
    private String   methodName;
    private String type;
    
    ItemAttributesEnum (String methodName, String type) {
        this.methodName = methodName;
        this.type = type;
    }
    
    public Object getValue(Item item) {
        try {
            Class<?> itemClass = Item.class;
            Method method = itemClass.getDeclaredMethod(methodName);
            return method.invoke(item);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace(); // TODO : replace by log
            return null;
        }
    }
    
    public String getType() {
        return type;
    }
    
}
