package fr.d4rt4gnan.openqia.items.model;


import fr.d4rt4gnan.openqia.items.technical.utils.GenericField;


/**
 * Item's value added tax.
 *
 * @author D4RT4GNaN
 * @since 07/02/2022
 */
public class ValueAddedTax extends GenericField<Double> {
    
    public ValueAddedTax () {
        this.set(0.0);
    }
    
}
