package fr.d4rt4gnan.openqia.items.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;
import java.util.UUID;


/**
 * @author D4RT4GNaN
 * @since 18/01/2022
 */
@Entity
@Table(name = "items", schema = "pgitems")
public class ItemBean {
    
    /**
     * The unique identifier of the ItemBean object in Java and in
     * the Database.
     */
    private UUID reference;
    
    /**
     * The designation of the item.
     */
    private String designation;
    
    /**
     * The unit price of the item.
     */
    private Double unitPrice;
    
    /**
     * The value added tax of the item.
     */
    private Double valueAddedTax;
    
    public ItemBean () {
        this.reference = UUID.randomUUID();
        this.designation = "";
        this.unitPrice = 0.0;
        this.valueAddedTax = 0.0;
    }
    
    /**
     * Getter of the "reference" attribute.
     *
     * @return the value of the "reference" attribute.
     */
    @Id
    @Column(name = "reference")
    public UUID getReference () {
        return reference;
    }
    
    /**
     * Setter of the "reference" attribute.
     *
     * @param reference - The value assigned.
     */
    public void setReference (final UUID reference) {
        this.reference = reference;
    }
    
    /**
     * Getter of the "designation" attribute.
     *
     * @return the value of the "designation" attribute.
     */
    @Column(name = "designation")
    public String getDesignation () {
        return designation;
    }
    
    /**
     * Setter of the "designation" attribute.
     *
     * @param designation - The value assigned.
     */
    public void setDesignation (final String designation) {
        this.designation = designation;
    }
    
    /**
     * Getter of the "unitPrice" attribute.
     *
     * @return the value of the "unitPrice" attribute.
     */
    @Column(name = "unit_price")
    public Double getUnitPrice () {
        return unitPrice;
    }
    
    /**
     * Setter of the "unitPrice" attribute.
     *
     * @param unitPrice - The value assigned.
     */
    public void setUnitPrice (final Double unitPrice) {
        this.unitPrice = unitPrice;
    }
    
    /**
     * Getter of the "valueAddedTax" attribute.
     *
     * @return the value of the "valueAddedTax" attribute.
     */

    @Column(name = "value_added_tax")
    public Double getValueAddedTax () {
        return valueAddedTax;
    }
    
    /**
     * Setter of the "valueAddedTax" attribute.
     *
     * @param valueAddedTax - The value assigned.
     */
    public void setValueAddedTax (final Double valueAddedTax) {
        this.valueAddedTax = valueAddedTax;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ItemBean itemBean = (ItemBean) o;
        return Objects.equals(this.reference, itemBean.reference)
               && Objects.equals(this.designation, itemBean.designation)
               && Objects.equals(this.unitPrice, itemBean.unitPrice)
               && Objects.equals(this.valueAddedTax, itemBean.valueAddedTax);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public int hashCode() {
        return Objects.hash(reference, designation, unitPrice, valueAddedTax);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public String toString() {
        return "class ItemBean {\n" + "    reference: " + reference + "\n" + "    designation: " + designation + "\n" +
               "    unit price: " + unitPrice + "\n" + "    value added tax: " + valueAddedTax + "\n" + "}";
    }
    
}
