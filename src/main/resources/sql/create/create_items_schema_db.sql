CREATE SCHEMA IF NOT EXISTS pgitems AUTHORIZATION postgres;

SET search_path TO extensions, pgitems;

CREATE TABLE IF NOT EXISTS pgitems.items
(
    reference uuid DEFAULT uuid_generate_v4(),
    designation VARCHAR NOT NULL,
    unit_price DOUBLE PRECISION,
    value_added_tax DOUBLE PRECISION,
    PRIMARY KEY (reference)
);

COMMENT ON TABLE pgitems.items IS 'Contains all informations about items.';
COMMENT ON COLUMN pgitems.items.reference IS 'The table''s arbitrary primary key.';
COMMENT ON COLUMN pgitems.items.designation IS 'The item''s label.';
COMMENT ON COLUMN pgitems.items.unit_price IS 'The item''s unit price.';
COMMENT ON COLUMN pgitems.items.value_added_tax IS 'The VAT applied on the item.';