package fr.d4rt4gnan.openqia.items.business;


import fr.d4rt4gnan.openqia.items.dao.ItemsRepository;
import fr.d4rt4gnan.openqia.items.model.ItemBean;
import fr.d4rt4gnan.openqia.items.technical.mappers.ItemsMapper;
import fr.d4rt4gnan.openqia.items.model.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


/**
 * @author D4RT4GNaN
 * @since 18/01/2022
 */
@ExtendWith(MockitoExtension.class)
class ItemsServiceTest {
    
    @InjectMocks
    private ItemsService classUnderTest;
    
    @Mock
    private ItemsMapper     itemsMapper;
    @Mock
    private ItemsRepository itemsRepository;
    
    private UUID reference = UUID.randomUUID();
    
    @Test
    void givenItemInformation_whenCallingAddItemMethod_thenItReturnResponse201WithTheSavedItemInformation() {
        // GIVEN
        ItemBean itemBean = new ItemBean();
        itemBean.setReference(reference);
    
        Item item = new Item();
        item.setReference(reference);
        
        when(itemsMapper.toItemBean(any())).thenReturn(itemBean);
        when(itemsRepository.save(any())).thenReturn(itemBean);
        when(itemsMapper.toItem(any())).thenReturn(item);
        
        ResponseEntity<Item> expected = new ResponseEntity<>(item, HttpStatus.CREATED);
        
        // WHEN
        ResponseEntity<Item> result = classUnderTest.addItem(item);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAnExistingID_whenCallingGetItemMethod_thenItReturnTheGoodItem() {
        // GIVEN
        ItemBean itemBean = new ItemBean();
        itemBean.setReference(reference);
        
        Item item = new Item();
        item.setReference(reference);
        
        when(itemsRepository.findById(any())).thenReturn(java.util.Optional.of(itemBean));
        when(itemsMapper.toItem(any())).thenReturn(item);
        
        ResponseEntity<Item> expected = new ResponseEntity<>(item, HttpStatus.OK);
        
        // WHEN
        ResponseEntity<Item> result = classUnderTest.getItem(reference);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAWrongID_whenCallingGetItemMethod_thenItReturnAResponseNotFound() {
        // GIVEN
        when(itemsRepository.findById(any())).thenReturn(Optional.empty());
        
        ResponseEntity<Item> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
        // WHEN
        ResponseEntity<Item> result = classUnderTest.getItem(reference);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenNothing_whenCallingGetItemsMethod_thenItReturnAResponse200() {
        // GIVEN
        List<Item> items = new ArrayList<>();
        
        when(itemsRepository.findItemBeansByDesignationIgnoringCaseContaining(anyString(), any())).thenReturn(Page.empty());
        when(itemsMapper.toItems(any())).thenReturn(items);
        
        // WHEN
        ResponseEntity<List<Item>> result = classUnderTest.getItems("prise", 5);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    
    @Test
    void givenNothing_whenCallingGetItemsMethod_thenItReturnAnEmptyList() {
        // GIVEN
        List<Item> expected = new ArrayList<>();
        
        when(itemsRepository.findItemBeansByDesignationIgnoringCaseContaining(anyString(), any())).thenReturn(Page.empty());
        when(itemsMapper.toItems(any())).thenReturn(expected);
        
        // WHEN
        ResponseEntity<List<Item>> result = classUnderTest.getItems("prise", 5);
        
        // THEN
        assertThat(result.getBody()).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenItemWhoExists_whenCallingUpdateItemMethod_thenItReturnAResponseEntityWithCode201() {
        // GIVEN
        UUID reference = UUID.randomUUID();
        
        Item item = new Item();
        item.setReference(reference);
        
        ItemBean itemBean = new ItemBean();
        
        when(itemsRepository.findById(reference)).thenReturn(Optional.of(itemBean));
        when(itemsMapper.updateItemBean(any(), any())).thenReturn(itemBean);
        when(itemsRepository.save(any())).thenReturn(itemBean);
        when(itemsMapper.toItem(any())).thenReturn(item);
        
        // WHEN
        ResponseEntity<Item> result = classUnderTest.updateItem(reference, item);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
    
    @Test
    void givenAnItemThatDoesNotExist_whenCallingUpdateItemMethod_thenItReturnAResponseEntityWithCode404() {
        // GIVEN
        UUID reference = UUID.randomUUID();
        
        Item item = new Item();
        item.setReference(reference);
        
        when(itemsRepository.findById(reference)).thenReturn(Optional.empty());
        
        // WHEN
        ResponseEntity<Item> result = classUnderTest.updateItem(reference, item);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
    
    @Test
    void givenAnExistingItem_whenCallingCalculationItemTotalPriceMethod_thenItReturn24() {
        // GIVEN
        UUID reference = UUID.randomUUID();
        
        Item item = new Item();
        item.setReference(reference);
        item.setDesignation("Prise 12V");
        item.setUnitPrice(20.0);
        item.setQuantity(1);
        item.setValueAddedTax(0.20);
    
        ItemBean itemBean = new ItemBean();
        itemBean.setReference(reference);
        itemBean.setDesignation("Prise 12V");
        itemBean.setUnitPrice(20.0);
        
        when(itemsRepository.findById(reference)).thenReturn(Optional.of(itemBean));
        when(itemsMapper.toNewValue(item, itemBean)).thenReturn(item);
        
        // WHEN
        ResponseEntity<Item> result = classUnderTest.calculationItemTotalPrice(reference, item);
        
        // THEN
        assertThat(Objects.requireNonNull(result.getBody()).getTotalPrice()).isEqualTo(24.0);
    }
    
    @Test
    void givenAnItemThatDoesNotExist_whenCallingCalculationItemTotalPriceMethod_thenItReturn24() {
        // GIVEN
        UUID reference = UUID.randomUUID();
        
        Item item = new Item();
        item.setReference(reference);
        item.setDesignation("Prise 12V");
        item.setUnitPrice(20.0);
        item.setQuantity(1);
        item.setValueAddedTax(0.20);
        
        when(itemsRepository.findById(reference)).thenReturn(Optional.empty());
        when(itemsMapper.toValidItem(item)).thenReturn(item);
        
        // WHEN
        ResponseEntity<Item> result = classUnderTest.calculationItemTotalPrice(reference, item);
        
        // THEN
        assertThat(Objects.requireNonNull(result.getBody()).getTotalPrice()).isEqualTo(24.0);
    }
    
}
