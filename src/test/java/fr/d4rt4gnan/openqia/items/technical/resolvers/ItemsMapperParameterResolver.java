package fr.d4rt4gnan.openqia.items.technical.resolvers;


import fr.d4rt4gnan.openqia.items.technical.mappers.ItemsMapper;
import fr.d4rt4gnan.openqia.items.technical.mappers.impl.ItemsMapperImpl;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;


/**
 * @author D4RT4GNaN
 * @since 18/01/2022
 */
public class ItemsMapperParameterResolver implements ParameterResolver {
    
    @Override
    public boolean supportsParameter (
            ParameterContext parameterContext, ExtensionContext extensionContext
    ) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == ItemsMapper.class;
    }
    
    @Override
    public Object resolveParameter (
            ParameterContext parameterContext, ExtensionContext extensionContext
    ) throws ParameterResolutionException {
        return new ItemsMapperImpl();
    }
    
}
