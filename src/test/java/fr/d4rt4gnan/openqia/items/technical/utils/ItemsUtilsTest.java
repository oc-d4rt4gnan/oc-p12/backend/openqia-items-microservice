package fr.d4rt4gnan.openqia.items.technical.utils;


import fr.d4rt4gnan.openqia.items.technical.resolvers.ItemsUtilsParameterResolver;
import fr.d4rt4gnan.openqia.items.technical.utils.enums.ItemAttributesEnum;
import fr.d4rt4gnan.openqia.items.model.Item;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


/**
 * @author D4RT4GNaN
 * @since 27/02/2022
 */
@ExtendWith({ ItemsUtilsParameterResolver.class, MockitoExtension.class })
class ItemsUtilsTest {
    
    @Nested
    class IsValid {
        
        @Test
        void givenItemAndAttributeNotNull_whenCallingIsValidMethod_thenReturnTrue (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setQuantity(1);
            
            ItemAttributesEnum attribute = ItemAttributesEnum.QUANTITY;
            
            // WHEN
            boolean result = itemsUtils.isValid(item, attribute);
            
            // THEN
            assertThat(result).isTrue();
        }
    
        @Test
        void givenItemNullAndAttributeNotNull_whenCallingIsValidMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = null;
    
            ItemAttributesEnum attribute = ItemAttributesEnum.QUANTITY;
    
            // WHEN
            boolean result = itemsUtils.isValid(item, attribute);
    
            // THEN
            assertThat(result).isFalse();
        }
    
        @Test
        void givenItemNotNullAndAttributeNull_whenCallingIsValidMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
    
            ItemAttributesEnum attribute = null;
    
            // WHEN
            boolean result = itemsUtils.isValid(item, attribute);
    
            // THEN
            assertThat(result).isFalse();
        }
    
        @Test
        void givenItemNotNullAndAttributeEmpty_whenCallingIsValidMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
    
            ItemAttributesEnum attribute = ItemAttributesEnum.UNIT_PRICE;
    
            // WHEN
            boolean result = itemsUtils.isValid(item, attribute);
    
            // THEN
            assertThat(result).isFalse();
        }
    }
    
    @Nested
    class IsNotBlank {
        
        @Test
        void givenAValidAndNotBlankString_whenCallingIsNotBlankMethod_thenReturnTrue (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setDesignation("Prise sans terre");
            
            ItemAttributesEnum attribute = ItemAttributesEnum.DESIGNATION;
    
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
            
            // WHEN
            boolean result = itemsUtilsSpy.isNotBlank(item, attribute);
            
            // THEN
            assertThat(result).isTrue();
        }
    
        @Test
        void givenAnInvalidAndNotBlankString_whenCallingIsNotBlankMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
        
            ItemAttributesEnum attribute = ItemAttributesEnum.DESIGNATION;
        
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(false);
        
            // WHEN
            boolean result = itemsUtilsSpy.isNotBlank(item, attribute);
        
            // THEN
            assertThat(result).isFalse();
        }
    
        @Test
        void givenAValidInteger_whenCallingIsNotBlankMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setQuantity(1);
        
            ItemAttributesEnum attribute = ItemAttributesEnum.QUANTITY;
        
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
        
            // WHEN
            boolean result = itemsUtilsSpy.isNotBlank(item, attribute);
        
            // THEN
            assertThat(result).isFalse();
        }
    
        @Test
        void givenAValidBlankString_whenCallingIsNotBlankMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setDesignation("   ");
        
            ItemAttributesEnum attribute = ItemAttributesEnum.DESIGNATION;
        
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
        
            // WHEN
            boolean result = itemsUtilsSpy.isNotBlank(item, attribute);
        
            // THEN
            assertThat(result).isFalse();
        }
    
        @Test
        void givenAValidEmptyString_whenCallingIsNotBlankMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setDesignation("");
        
            ItemAttributesEnum attribute = ItemAttributesEnum.DESIGNATION;
        
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
        
            // WHEN
            boolean result = itemsUtilsSpy.isNotBlank(item, attribute);
        
            // THEN
            assertThat(result).isFalse();
        }
    }
    
    @Nested
    class DoubleIsNotZero {
        
        @Test
        void givenAValidAndPositiveDouble_whenCallingDoubleIsNotZeroMethod_thenReturnTrue (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setUnitPrice(20.0);
            
            ItemAttributesEnum attribute = ItemAttributesEnum.UNIT_PRICE;
            
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
            
            // WHEN
            boolean result = itemsUtilsSpy.doubleIsNotZero(item, attribute);
            
            // THEN
            assertThat(result).isTrue();
        }
    
        @Test
        void givenAInvalidAndPositiveDouble_whenCallingDoubleIsNotZeroMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
        
            ItemAttributesEnum attribute = ItemAttributesEnum.UNIT_PRICE;
        
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(false);
        
            // WHEN
            boolean result = itemsUtilsSpy.doubleIsNotZero(item, attribute);
        
            // THEN
            assertThat(result).isFalse();
        }
    
        @Test
        void givenAValidString_whenCallingDoubleIsNotZeroMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setDesignation("designation");
        
            ItemAttributesEnum attribute = ItemAttributesEnum.DESIGNATION;
        
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
        
            // WHEN
            boolean result = itemsUtilsSpy.doubleIsNotZero(item, attribute);
        
            // THEN
            assertThat(result).isFalse();
        }
    
        @Test
        void givenAValidDoubleEqualToZero_whenCallingDoubleIsNotZeroMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setUnitPrice(0.0);
        
            ItemAttributesEnum attribute = ItemAttributesEnum.UNIT_PRICE;
        
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
        
            // WHEN
            boolean result = itemsUtilsSpy.doubleIsNotZero(item, attribute);
        
            // THEN
            assertThat(result).isFalse();
        }
    }
    
    @Nested
    class IntIsNotZero {
        
        @Test
        void givenAValidAndPositiveInteger_whenCallingIntIsNotZeroMethod_thenReturnTrue (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setQuantity(1);
            
            ItemAttributesEnum attribute = ItemAttributesEnum.QUANTITY;
            
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
            
            // WHEN
            boolean result = itemsUtilsSpy.intIsNotZero(item, attribute);
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        void givenAInvalidAndPositiveInteger_whenCallingIntIsNotZeroMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            
            ItemAttributesEnum attribute = ItemAttributesEnum.QUANTITY;
            
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(false);
            
            // WHEN
            boolean result = itemsUtilsSpy.intIsNotZero(item, attribute);
            
            // THEN
            assertThat(result).isFalse();
        }
        
        @Test
        void givenAValidString_whenCallingIntIsNotZeroMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setDesignation("designation");
            
            ItemAttributesEnum attribute = ItemAttributesEnum.DESIGNATION;
            
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
            
            // WHEN
            boolean result = itemsUtilsSpy.intIsNotZero(item, attribute);
            
            // THEN
            assertThat(result).isFalse();
        }
        
        @Test
        void givenAValidIntegerEqualToZero_whenCallingIntIsNotZeroMethod_thenReturnFalse (final ItemsUtils itemsUtils) {
            // GIVEN
            Item item = new Item();
            item.setQuantity(0);
            
            ItemAttributesEnum attribute = ItemAttributesEnum.QUANTITY;
            
            ItemsUtils itemsUtilsSpy = Mockito.spy(itemsUtils);
            when(itemsUtilsSpy.isValid(any(), any())).thenReturn(true);
            
            // WHEN
            boolean result = itemsUtilsSpy.intIsNotZero(item, attribute);
            
            // THEN
            assertThat(result).isFalse();
        }
    }
    
}
