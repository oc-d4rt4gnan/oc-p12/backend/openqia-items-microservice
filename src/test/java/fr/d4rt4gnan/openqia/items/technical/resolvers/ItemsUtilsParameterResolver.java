package fr.d4rt4gnan.openqia.items.technical.resolvers;


import fr.d4rt4gnan.openqia.items.technical.utils.ItemsUtils;
import fr.d4rt4gnan.openqia.items.technical.utils.impl.ItemsUtilsImpl;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;


/**
 * @author D4RT4GNaN
 * @since 27/02/2022
 */
public class ItemsUtilsParameterResolver implements ParameterResolver {
    
    @Override
    public boolean supportsParameter (
            ParameterContext parameterContext, ExtensionContext extensionContext
    ) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == ItemsUtils.class;
    }
    
    @Override
    public Object resolveParameter (
            ParameterContext parameterContext, ExtensionContext extensionContext
    ) throws ParameterResolutionException {
        return new ItemsUtilsImpl();
    }
    
}
