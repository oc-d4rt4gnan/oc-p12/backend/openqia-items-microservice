package fr.d4rt4gnan.openqia.items.technical.mappers;

import fr.d4rt4gnan.openqia.items.model.ItemBean;
import fr.d4rt4gnan.openqia.items.technical.mappers.impl.ItemsMapperImpl;
import fr.d4rt4gnan.openqia.items.technical.resolvers.ItemsMapperParameterResolver;
import fr.d4rt4gnan.openqia.items.technical.utils.ItemsUtils;
import fr.d4rt4gnan.openqia.items.model.Item;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;


/**
 * @author D4RT4GNaN
 * @since 18/01/2022
 */
@ExtendWith({ ItemsMapperParameterResolver.class, MockitoExtension.class })
class ItemsMapperTest {
    
    private UUID   reference   = UUID.randomUUID();
    private String designation = "designation";
    private double unitPrice   = 10.0;
    
    @Mock
    private ItemsUtils itemsUtils;
    
    @InjectMocks
    private ItemsMapper classUnderTest = new ItemsMapperImpl();
    
    @Nested
    class ToItemBean {
        @Test
        void givenItemInformation_whenCallingToItemBeanMethod_thenGettingInformationInItemBeanFormat () {
            // GIVEN
            Item item = new Item();
            item.setReference(reference);
            item.setDesignation(designation);
            item.setUnitPrice(unitPrice);
            
            ItemBean expected = new ItemBean();
            expected.setReference(reference);
            expected.setDesignation(designation);
            expected.setUnitPrice(unitPrice);
            
            // WHEN
            ItemBean result = classUnderTest.toItemBean(item);
            
            // THEN
            assertThat(result).usingRecursiveComparison().isEqualTo(expected);
        }
    }
    
    @Nested
    class ToItem {
        @Test
        void givenItemBeanInformation_whenCallingToItemMethod_thenGettingInformationInItemFormat () {
            // GIVEN
            ItemBean itemBean = new ItemBean();
            itemBean.setReference(reference);
            itemBean.setDesignation(designation);
            itemBean.setUnitPrice(unitPrice);
        
            Item expected = new Item();
            expected.setReference(reference);
            expected.setDesignation(designation);
            expected.setUnitPrice(unitPrice);
            
            // WHEN
            Item result = classUnderTest.toItem(itemBean);
            
            // THEN
            assertThat(result).usingRecursiveComparison().isEqualTo(expected);
        }
    }
    
    @Nested
    class ToItems {
        @Test
        void givenItemBeanInformation_whenCallingToItemsMethod_thenGettingInformationInItemFormat () {
            // GIVEN
            ItemBean itemBean = new ItemBean();
            itemBean.setReference(reference);
            itemBean.setDesignation("Prise");
            itemBean.setUnitPrice(10.0);
            
            UUID reference2 = UUID.randomUUID();
            
            ItemBean itemBean2 = new ItemBean();
            itemBean2.setReference(reference2);
            itemBean2.setDesignation("Interrupteur");
            itemBean2.setUnitPrice(20.0);
            
            List<ItemBean> itemBeansList = Arrays.asList(itemBean, itemBean2);
            Page<ItemBean> itemBeans = new PageImpl<>(itemBeansList);
            
            Item item = new Item();
            item.setReference(reference);
            item.setDesignation("Prise");
            item.setUnitPrice(10.0);
            
            Item item2 = new Item();
            item2.setReference(reference2);
            item2.setDesignation("Interrupteur");
            item2.setUnitPrice(20.0);
            
            List<Item> expected = Arrays.asList(item, item2);
            
            // WHEN
            List<Item> result = classUnderTest.toItems(itemBeans);
            
            // THEN
            assertThat(result).usingRecursiveComparison().isEqualTo(expected);
        }
    }
    
    @Nested
    class UpdateDesignation {
        @Test
        void givenAValidInput_whenCallingUpdateDesignationMethod_thenReturnDesignationFromFront () {
            // GIVEN
            ItemBean itemBean = new ItemBean();
            
            Item item = new Item();
            item.setDesignation("Prise");
            
            when(itemsUtils.isNotBlank(any(), any())).thenReturn(true);
            
            // WHEN
            String result = classUnderTest.updateDesignation(item, itemBean);
            
            // THEN
            assertThat(result).isEqualTo("Prise");
        }
    
        @Test
        void givenAnInvalidInput_whenCallingUpdateDesignationMethod_thenReturnDesignationFromDatabase () {
            // GIVEN
            ItemBean itemBean = new ItemBean();
            itemBean.setDesignation("Prise");
        
            Item item = new Item();
            item.setDesignation("");
        
            when(itemsUtils.isNotBlank(any(), any())).thenReturn(false);
        
            // WHEN
            String result = classUnderTest.updateDesignation(item, itemBean);
        
            // THEN
            assertThat(result).isEqualTo("Prise");
        }
    }
    
    @Nested
    class UpdateUnitPrice {
        @Test
        void givenAValidInput_whenCallingUpdateUnitPriceMethod_thenReturnTheUnitPriceFromFront () {
            // GIVEN
            ItemBean itemBean = new ItemBean();
            
            Item item = new Item();
            item.setUnitPrice(20.0);
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(true);
            
            // WHEN
            double result = classUnderTest.updateUnitPrice(item, itemBean);
            
            // THEN
            assertThat(result).isEqualTo(20.0);
        }
        
        @Test
        void givenAnInvalidInput_whenCallingUpdateUnitPriceMethod_thenReturnTheUnitPriceFromDatabase () {
            // GIVEN
            ItemBean itemBean = new ItemBean();
            itemBean.setUnitPrice(20.0);
            
            Item item = new Item();
            item.setUnitPrice(0.0);
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(false);
            
            // WHEN
            double result = classUnderTest.updateUnitPrice(item, itemBean);
            
            // THEN
            assertThat(result).isEqualTo(20.0);
        }
    }
    
    @Nested
    class UpdateValueAddedTax {
        @Test
        void givenAValidInput_whenCallingUpdateValueAddedTaxMethod_thenReturnTheValueAddedTaxFromFront () {
            // GIVEN
            ItemBean itemBean = new ItemBean();
            
            Item item = new Item();
            item.setValueAddedTax(0.20);
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(true);
            
            // WHEN
            double result = classUnderTest.updateValueAddedTax(item, itemBean);
            
            // THEN
            assertThat(result).isEqualTo(0.20);
        }
        
        @Test
        void givenAnInvalidInput_whenCallingUpdateValueAddedTaxMethod_thenReturnTheValueAddedTaxFromDatabase () {
            // GIVEN
            ItemBean itemBean = new ItemBean();
            itemBean.setValueAddedTax(0.20);
            
            Item item = new Item();
            item.setValueAddedTax(0.0);
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(false);
            
            // WHEN
            double result = classUnderTest.updateValueAddedTax(item, itemBean);
            
            // THEN
            assertThat(result).isEqualTo(0.20);
        }
    }
    
    @Nested
    class CheckQuantity {
        @Test
        void givenAValidInput_whenCallingCheckQuantityMethod_thenReturnTheQuantityFromFront () {
            // GIVEN
            Item item = new Item();
            item.setQuantity(3);
            
            when(itemsUtils.intIsNotZero(any(), any())).thenReturn(true);
            
            // WHEN
            int result = classUnderTest.checkQuantity(item);
            
            // THEN
            assertThat(result).isEqualTo(3);
        }
        
        @Test
        void givenAnInvalidInput_whenCallingCheckQuantityMethod_thenReturnTheDefaultValue () {
            // GIVEN
            Item item = new Item();
            
            when(itemsUtils.intIsNotZero(any(), any())).thenReturn(false);
            
            // WHEN
            int result = classUnderTest.checkQuantity(item);
            
            // THEN
            assertThat(result).isEqualTo(1);
        }
    }
    
    @Nested
    class CheckTotalPrice {
        @Test
        void givenAValidInput_whenCallingCheckTotalPriceMethod_thenReturnTheTotalPriceFromFront () {
            // GIVEN
            Item item = new Item();
            item.setTotalPrice(30.0);
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(true);
            
            // WHEN
            double result = classUnderTest.checkTotalPrice(item);
            
            // THEN
            assertThat(result).isEqualTo(30.0);
        }
        
        @Test
        void givenAnInvalidInput_whenCallingCheckTotalPriceMethod_thenReturnTheDefaultValue () {
            // GIVEN
            Item item = new Item();
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(false);
            
            // WHEN
            double result = classUnderTest.checkTotalPrice(item);
            
            // THEN
            assertThat(result).isEqualTo(0.0);
        }
    }
    
    @Nested
    class CheckUnitPrice {
        @Test
        void givenAValidInput_whenCallingCheckUnitPriceMethod_thenReturnTheUnitPriceFromFront () {
            // GIVEN
            Item item = new Item();
            item.setUnitPrice(30.0);
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(true);
            
            // WHEN
            double result = classUnderTest.checkUnitPrice(item);
            
            // THEN
            assertThat(result).isEqualTo(30.0);
        }
        
        @Test
        void givenAnInvalidInput_whenCallingCheckUnitPriceMethod_thenReturnTheDefaultValue () {
            // GIVEN
            Item item = new Item();
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(false);
            
            // WHEN
            double result = classUnderTest.checkUnitPrice(item);
            
            // THEN
            assertThat(result).isEqualTo(0.0);
        }
    }
    
    @Nested
    class CheckValueAddedTax {
        @Test
        void givenAValidInput_whenCallingCheckValueAddedTaxMethod_thenReturnTheValueAddedTaxPriceFromFront () {
            // GIVEN
            Item item = new Item();
            item.setValueAddedTax(0.20);
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(true);
            
            // WHEN
            double result = classUnderTest.checkValueAddedTax(item);
            
            // THEN
            assertThat(result).isEqualTo(0.20);
        }
        
        @Test
        void givenAnInvalidInput_whenCallingCheckValueAddedTaxMethod_thenReturnTheDefaultValue () {
            // GIVEN
            Item item = new Item();
            
            when(itemsUtils.doubleIsNotZero(any(), any())).thenReturn(false);
            
            // WHEN
            double result = classUnderTest.checkTotalPrice(item);
            
            // THEN
            assertThat(result).isEqualTo(0.0);
        }
    }
    
    @Nested
    class ToValidItem {
        @Test
        void givenAValidItem_whenCallingToValidItemMethod_thenReturnTheUpdatedItem () {
            // GIVEN
            UUID reference = UUID.randomUUID();
            
            Item item = new Item();
            item.setReference(reference);
            item.setDesignation("Prise");
            item.setQuantity(2);
            item.setUnitPrice(14.90);
            item.setValueAddedTax(0.2);
            
            ItemsMapper itemsMapperSpy = Mockito.spy(classUnderTest);
            doReturn(14.90).when(itemsMapperSpy).checkUnitPrice(any());
            doReturn(0.2).when(itemsMapperSpy).checkValueAddedTax(any());
            doReturn(2).when(itemsMapperSpy).checkQuantity(any());
            
            // WHEN
            Item result = itemsMapperSpy.toValidItem(item);
            
            // THEN
            assertThat(result).usingRecursiveComparison().isEqualTo(item);
        }
        
        @Test
        void givenANullItem_whenCallingToValidItemMethod_thenReturnTheDefaultValues () {
            // GIVEN
            Item expected = new Item();
            expected.setQuantity(1);
            expected.setValueAddedTax(0.2);
            expected.setUnitPrice(0.0);
            
            // WHEN
            Item result = classUnderTest.toValidItem(null);
            
            // THEN
            assertThat(result).usingRecursiveComparison().isEqualTo(expected);
        }
    }
    
    @Test
    void givenItemUsableToUpdate_whenCallingUpdateItemBeanMethod_thenItReturnItemBeanUpdated (
            ItemsMapper classUnderTest
    ) {
        // GIVEN
        Item item = new Item();
        item.setReference(reference);
        item.setDesignation("Prise 2P+T");
        item.setUnitPrice(10.0);
    
        ItemBean itemBean = new ItemBean();
        itemBean.setReference(reference);
        itemBean.setDesignation("Prise 2P+T");
        itemBean.setUnitPrice(20.0);
    
        ItemBean expected = new ItemBean();
        expected.setReference(reference);
        expected.setDesignation("Prise 2P+T");
        expected.setUnitPrice(10.0);
        
        // WHEN
        ItemBean result = classUnderTest.updateItemBean(item, itemBean);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenItemUnusableToUpdate_whenCallingUpdateItemBeanMethod_thenItReturnItemBean (
            ItemsMapper classUnderTest
    ) {
        // GIVEN
        Item item = new Item();
        item.setReference(reference);
        item.setDesignation("");
        item.setUnitPrice(null);
    
        ItemBean itemBean = new ItemBean();
        itemBean.setReference(reference);
        itemBean.setDesignation("Prise 2P+T");
        itemBean.setUnitPrice(20.0);
    
        // WHEN
        ItemBean result = classUnderTest.updateItemBean(item, itemBean);
    
        // THEN
        assertThat(result).isEqualTo(itemBean);
    }
    
}
